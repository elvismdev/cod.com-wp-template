<?php

/*
*
*	Dante Functions - Child Theme
*	------------------------------------------------
*	These functions will override the parent theme
*	functions. We have provided some examples below.
*
*
*/


	// OVERWRITE PAGE BUILDER ASSETS
//	function spb_regsiter_assets() {
//		require_once( get_stylesheet_directory_uri() . '/default.php' );
//	}
//	if (is_admin()) {
//	add_action('admin_init', 'spb_regsiter_assets', 2);
//	}
//	if (!is_admin()) {
//	add_action('wp', 'spb_regsiter_assets', 2);
//	}


// Add custom assets
function cod_extra_assets() {
	wp_enqueue_style( 'bootstrapvalidator', get_stylesheet_directory_uri() . '/bootstrapvalidator/dist/css/bootstrapValidator.min.css' );
	wp_enqueue_script( 'bootstrapvalidator', get_stylesheet_directory_uri() . '/bootstrapvalidator/dist/js/bootstrapValidator.min.js', array('jquery'), '0.5.3', true );
}
add_action( 'wp_enqueue_scripts', 'cod_extra_assets' );


// Hook the modals with the SingUp forms with their validation.
function cod_hook_modal_forms_and_validations() { ?>
<script type="text/javascript">

	function isMobile() {
		try{ document.createEvent('TouchEvent'); return true; }
		catch(e){ return false; }
	}

	Shadowbox.init({
		handleOversize: "drag",
		enableKeys: false,
		onFinish: function ()
		{
			jQuery('#getStartedForm, #newHireTrainingForm, #salesMeetingsForm, #roadToSaleForm, #handlingObjectionsForm, #phonesForm, #followUpForm, #prospectingForm, #internetLeadsForm, #closingAndNegotiatingForm, #motivationForm, #successTraitsForm, #managementTrainingForm, #rolePlayingForm, #serviceAdvisorForm, #hiringForm, #discForm')
			.bootstrapValidator({
				fields: {
					inf_field_FirstName: {
						group: '.one_half',
						validators: {
							notEmpty: {
								message: 'First Name is required'
							}
						}
					},
					inf_field_LastName: {
						group: '.one_half',
						validators: {
							notEmpty: {
								message: 'Last Name is required'
							}
						}
					},
					inf_field_Company: {
						group: '.one_half',
						validators: {
							notEmpty: {
								message: 'Organization is required'
							}
						}
					},
					inf_field_JobTitle: {
						group: '.one_half',
						validators: {
							notEmpty: {
								message: 'Role is required'
							}
						}
					},
					inf_field_Email: {
						group: '.one_half',
						validators: {
							notEmpty: {
								message: 'Email is required'
							},
							emailAddress: {
								message: 'Still not a valid email address'
							}
						}
					},
					inf_field_Phone1: {
						group: '.one_half',
						validators: {
							notEmpty: {
								message: 'Phone is required'
							}
						}
					}
				}
			})
.on('success.form.bv', function(e) {

	console.log('Sending tracking data to GA and Adwords...');

	// IF THE FORM IS ALL VALID, HERE WE SEND THE TRACKING DATA TO GA AND ADWORDS.
	ga('send', 'event', 'LeadSubmission', 'GetFreeAccess');
	goog_report_conversion();

	// goog_report_conversion(933711372, 'YcgRCL7UrGEQjJydvQM');

});

if ( isMobile() === true ) {
	jQuery( '#sb-wrapper-inner' ).css( 'height', jQuery( '#sb-container form' ).height() + 200 );
}

jQuery( '#sb-overlay' ).css( 'height', jQuery( document ).height() );

}
});


// The modal forms with Shadowbox.
jQuery(function() {
	jQuery('#get-started').bind('click', function(event) {
		Shadowbox.open({
			content: '<div class="modal">\
			<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
			<h4>Complete the form below to get Free Trial.</h4>\
			<form id="getStartedForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
				<input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" />\
				<input name="inf_form_name" type="hidden" value="CU A La Carte" />\
				<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
				<div class="one_half">\
					<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
				</div>\
				<div class="one_half last">\
					<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
				</div>\
				<div class="one_half">\
					<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
				</div>\
				<div class="one_half last">\
					<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
				</div>\
				<input type="hidden" name="emails[0].Label" value="Work" />\
				<div class="one_half">\
					<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
				</div>\
				<input type="hidden" name="phones[0].Label" value="Work" />\
				<div class="one_half last"><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
				</div>\
				<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
				<span>*These fields are required and must be valid.</span>\
				<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
				<input type="submit" value="SIGN UP NOW"/>\
			</form>\
		</div>',
		player:  "html",
		height: 490
	});
});
jQuery('#sales').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<iframe src="//player.vimeo.com/video/63107522?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="newHireTrainingForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/d20123bb1eabed8d1b433491576ee7de" method="post">\
			<input name="inf_form_xid" type="hidden" value="d20123bb1eabed8d1b433491576ee7de" />\
			<input name="inf_form_name" type="hidden" value="Sales" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 690
});
});
jQuery('#customer-service').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<iframe src="//player.vimeo.com/video/63111349?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="salesMeetingsForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/9fda814db227dda01941cff33c5d4387" method="post">\
			<input name="inf_form_xid" type="hidden" value="9fda814db227dda01941cff33c5d4387" />\
			<input name="inf_form_name" type="hidden" value="Customer Service" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 690
});
});
jQuery('#negotiations').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<iframe src="//player.vimeo.com/video/63103406?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="roadToSaleForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/33252dbc4ff688c23f5eaa9872ff6016" method="post">\
			<input name="inf_form_xid" type="hidden" value="33252dbc4ff688c23f5eaa9872ff6016" />\
			<input name="inf_form_name" type="hidden" value="Negotiations" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 690
});
});
jQuery('#mastering-phones').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<iframe src="//player.vimeo.com/video/63110269?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="handlingObjectionsForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/db0300dbaef7d88a3815465534304ec7" method="post">\
			<input name="inf_form_xid" type="hidden" value="db0300dbaef7d88a3815465534304ec7" />\
			<input name="inf_form_name" type="hidden" value="Mastering Phones" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 690
});
});
jQuery('#phones').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<iframe src="//player.vimeo.com/video/63109963?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="phonesForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Prospecting" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 690
});
});
jQuery('#follow-up').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="followUpForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/4760888c7e5a4edfbe839a8fa01570b4" method="post">\
			<input name="inf_form_xid" type="hidden" value="4760888c7e5a4edfbe839a8fa01570b4" />\
			<input name="inf_form_name" type="hidden" value="Communications" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#prospecting').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="prospectingForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Rules of Success" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#internet-leads').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="internetLeadsForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#closing').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="closingAndNegotiatingForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Rules of Success" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#motivation').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="motivationForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#success-traits').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="successTraitsForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#management').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="managementTrainingForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#role-playing').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="rolePlayingForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#service-advisor').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="serviceAdvisorForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#hiring').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="hiringForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
jQuery('#disc').bind('click', function(event) {
	Shadowbox.open({
		content: '<div class="modal">\
		<img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" />\
		<h4>Complete the form below to get Free Access.</h4>\
		<form id="discForm" name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" method="post">\
			<input type="hidden" name="inf_form_xid" value="64be4b3826b39023cd334f205f683f60" />\
			<input name="inf_form_name" type="hidden" value="Motivation" />\
			<input name="infusionsoft_version" type="hidden" value="1.33.0.43" />\
			<div class="one_half">\
				<input id="inf_field_FirstName" name="inf_field_FirstName" placeholder="First Name*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_LastName" name="inf_field_LastName" placeholder="Last Name*" type="text" />\
			</div>\
			<div class="one_half">\
				<input id="inf_field_Company" name="inf_field_Company" placeholder="Organization*" type="text" />\
			</div>\
			<div class="one_half last">\
				<input id="inf_field_JobTitle" name="inf_field_JobTitle" placeholder="Role*" type="text" />\
			</div>\
			<input type="hidden" name="emails[0].Label" value="Work" />\
			<div class="one_half">\
				<input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text" />\
			</div>\
			<input type="hidden" name="phones[0].Label" value="Work" />\
			<div class="one_half last">\
				<input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" />\
			</div>\
			<textarea id="inf_custom_Message" name="inf_custom_Message" placeholder="Background Information"></textarea>\
			<span>*These fields are required and must be valid.</span>\
			<input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/>\
			<input type="submit" value="SIGN UP NOW"/>\
		</form>\
	</div>',
	player:  "html",
	height: 490
});
});
});
</script>
<?php }
add_action( 'wp_footer', 'cod_hook_modal_forms_and_validations', 100 );



?>