<?php
	/*
	Template Name: University Page
	*/
?>

<?php get_header(); ?>



<!-- jQuery.js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="http://192.241.140.122/cardoneuniversity.com/public_html/wp-content/themes/dante-child/shadowbox/shadowbox.css">
<script type="text/javascript" src="http://192.241.140.122/cardoneuniversity.com/public_html/wp-content/themes/dante-child/shadowbox/shadowbox.js"></script>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,300,400italic,400,700italic,700">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:900">

<script type="text/javascript">
	Shadowbox.init({
	    handleOversize: "drag",
	    enableKeys: false
	});
</script>



<style type="text/css">
	.modal {
		position: relative;
		display: inline;
	}
	#sb-body-inner {
		padding: 20px;
	}
	#sb-wrapper-inner {
		border: none;
		-webkit-box-shadow: 0 5px 15px rgba(0,0,0,0.5);
		box-shadow: 0 5px 15px rgba(0,0,0,0.5);
	}
	input[type="text"], input[type="password"], textarea {
		width: 100%;
		margin-bottom: 12px;
	}
	textarea {
		height: 90px;
	}
	.modal h4 {
		margin-bottom: 10px;
	}
	.modal span {
		font-size: 12px;
	}
	input[type="submit"] {
		float: right;
		margin: 6px 0 0 0;
	}
	a.image-video span.icon-play {
		position: absolute;
		left: 0;
		top: -12px;
		filter: alpha(opacity=75);
		-moz-opacity: 0.75;
		-khtml-opacity: 0.75;
		opacity: 0.75;
	}
	a.image-video:hover span.icon-play {
		filter: alpha(opacity=90);
		-moz-opacity: 0.9;
		-khtml-opacity: 0.9;
		opacity: 0.9;
	}
	a.image-video .title-video {
		width: 100%;
		font-family: 'Roboto Condensed';
		font-weight: 900;
		color: #ffffff;
		font-size: 24px;
		position: relative;
		top: -40px;
		margin: 0 auto;
		text-align: center;
		text-transform: uppercase;
		filter: alpha(opacity=90);
		-moz-opacity: 0.9;
		-khtml-opacity: 0.9;
		opacity: 0.9;
	}

	.box-1, .box-3 {
		position: relative;
		top: -20px;
	}

	.intro-video {
		position: relative;
		top: -94px;
		z-index: 99;
	}
	.intro-video iframe {
		border: 20px solid #ffffff;
	}

	@media only screen and (max-width: 767px) {
		.intro-video {
			top: -20px;
		}
		.intro-video iframe {
			border: 10px solid #ffffff;
			height: 260px;
		}			
		.box-1 {
			position: relative;
			top: 10px;
		}
		.fw-row {
			padding: 20px 0 40px !important;
		}
		#logo img, #logo img.retina {
			max-width: 290px;
		}
	}

	.mobile-menu-show {
		visibility: hidden !important;
	}	


	.sf-button {
		font-family: "Roboto", sans-serif !important;
		font-weight: 900 !important;
		font-style: normal !important;
	}
	.sf-button:hover{
		background: #2e2e37 !important;
	}

	.free-trial {
		display: block;
		font-family: Roboto Condensed;
		font-weight: 900;
		font-size: 48px;
		color: #f47d44;
		margin: 0 0 7px;
		letter-spacing: -2px;
		line-height: 40px;
	}
</style>



<script type="text/javascript">
	$(function() {
		$('#get-started').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#sales"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 420
	        });
		});
		$('#sales').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78107670?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#sales"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#customer-service').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109584?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#customer-service"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#negotiations').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109664?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#negotiations"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#mastering-phones').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109609?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#mastering-phones"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#prospecting').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109663?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#prospecting"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#communications').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109713?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#communications"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#rules-of-success').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109721?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#rules-of-success"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
		$('#motivation').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109745?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow==" /><div class="one_half"><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text" /></div><div class="one_half last"><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text" /></div><div class="one_half"><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" /></div><div class="one_half last"><input id="insightly_role" name="Role" placeholder="Role*" type="text" /></div><input type="hidden" name="emails[0].Label" value="Work" /><div class="one_half"><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text" /></div><input type="hidden" name="phones[0].Label" value="Work" /><div class="one_half last"><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" /></div><textarea id="insightly_background" name="background" placeholder="Background Information"></textarea><span>*These fields are required and must be valid.</span><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/><input type="submit" value="REQUEST TRIAL"/></form></div>',
	            player:  "html",
	            height: 660
	        });
		});
	});
</script>

<script>
	function validateForm() {
		var x=document.forms["insightly_web_to_contact"]["FirstName"].value;
		if (x==null || x=="") {
			alert("First Name must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["LastName"].value;
		if (x==null || x=="") {
			alert("Last Mame must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["Organization"].value;
		if (x==null || x=="") {
			alert("Organization must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["Role"].value;
		if (x==null || x=="") {
			alert("Role must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["emails[0].Value"].value;
		if (x==null || x=="") {
			alert("Email must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["phones[0].Value"].value;
		if (x==null || x=="") {
			alert("Phone must be filled out.");
			return false;
		}
	}
</script>

	

<?php
	$options = get_option('sf_dante_options');
	
	$default_show_page_heading = $options['default_show_page_heading'];
	$default_page_heading_bg_alt = $options['default_page_heading_bg_alt'];
	$default_sidebar_config = $options['default_sidebar_config'];
	$default_left_sidebar = $options['default_left_sidebar'];
	$default_right_sidebar = $options['default_right_sidebar'];
	
	$pb_active = get_post_meta($post->ID, '_spb_js_status', true);
	$show_page_title = get_post_meta($post->ID, 'sf_page_title', true);
	$page_title_style = get_post_meta($post->ID, 'sf_page_title_style', true);
	$page_title = get_post_meta($post->ID, 'sf_page_title_one', true);
	$page_subtitle = get_post_meta($post->ID, 'sf_page_subtitle', true);
	$page_title_bg = get_post_meta($post->ID, 'sf_page_title_bg', true);
	$fancy_title_image = rwmb_meta('sf_page_title_image', 'type=image&size=full');
	$page_title_text_style = get_post_meta($post->ID, 'sf_page_title_text_style', true);
	$fancy_title_image_url = "";
	
	if ($show_page_title == "") {
		$show_page_title = $default_show_page_heading;
	}
	if ($page_title_bg == "") {
		$page_title_bg = $default_page_heading_bg_alt;
	}
	if ($page_title == "") {
		$page_title = get_the_title();
	}
	
	foreach ($fancy_title_image as $detail_image) {
		$fancy_title_image_url = $detail_image['url'];
		break;
	}
									
	if (!$fancy_title_image) {
		$fancy_title_image = get_post_thumbnail_id();
		$fancy_title_image_url = wp_get_attachment_url( $fancy_title_image, 'full' );
	}
	
	$sidebar_config = get_post_meta($post->ID, 'sf_sidebar_config', true);
	$left_sidebar = get_post_meta($post->ID, 'sf_left_sidebar', true);
	$right_sidebar = get_post_meta($post->ID, 'sf_right_sidebar', true);
	
	if ($sidebar_config == "") {
		$sidebar_config = $default_sidebar_config;
	}
	if ($left_sidebar == "") {
		$left_sidebar = $default_left_sidebar;
	}
	if ($right_sidebar == "") {
		$right_sidebar = $default_right_sidebar;
	}
	
	sf_set_sidebar_global($sidebar_config);
	
	$page_wrap_class = $post_class_extra = '';
	if ($sidebar_config == "left-sidebar") {
	$page_wrap_class = 'has-left-sidebar has-one-sidebar row';
	$post_class_extra = 'col-sm-8';
	} else if ($sidebar_config == "right-sidebar") {
	$page_wrap_class = 'has-right-sidebar has-one-sidebar row';
	$post_class_extra = 'col-sm-8';
	} else if ($sidebar_config == "both-sidebars") {
	$page_wrap_class = 'has-both-sidebars row';
	$post_class_extra = 'col-sm-9';
	} else {
	$page_wrap_class = 'has-no-sidebar';
	}
	
	$remove_breadcrumbs = get_post_meta($post->ID, 'sf_no_breadcrumbs', true);
	$remove_bottom_spacing = get_post_meta($post->ID, 'sf_no_bottom_spacing', true);
	$remove_top_spacing = get_post_meta($post->ID, 'sf_no_top_spacing', true);
	
	if ($remove_bottom_spacing) {
	$page_wrap_class .= ' no-bottom-spacing';
	}
	if ($remove_top_spacing) {
	$page_wrap_class .= ' no-top-spacing';
	}
	
	$options = get_option('sf_dante_options');
	$disable_pagecomments = false;
	if (isset($options['disable_pagecomments']) && $options['disable_pagecomments'] == 1) {
	$disable_pagecomments = true;
	}
?>

<?php if ($show_page_title) { ?>
<div class="container">
	<div class="row">
		<?php if ($page_title_style == "fancy") { ?>
		<?php if ($fancy_title_image_url != "") { ?>
		<div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_text_style; ?>-style fancy-image" style="background-image: url(<?php echo $fancy_title_image_url; ?>);">
		<?php } else { ?>
		<div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
		<?php } ?>
			<div class="heading-text">
				<h1 class="entry-title"><?php echo $page_title; ?></h1>
				<?php if ($page_subtitle) { ?>
				<h3><?php echo $page_subtitle; ?></h3>
				<?php } ?>
			</div>
		</div>
		<?php } else { ?>
		<div class="page-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
			<div class="heading-text">
				<h1 class="entry-title"><?php echo $page_title; ?></h1>
			</div>
			<?php 
				// BREADCRUMBS
				if (!$remove_breadcrumbs) {
					echo sf_breadcrumbs();
				}
			?>
		</div>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php if ($sidebar_config != "no-sidebars" || $pb_active != "true") { ?>
<div class="container">
<?php } ?>

<div class="inner-page-wrap <?php echo $page_wrap_class; ?> clearfix">
		
	<?php if (have_posts()) : the_post(); ?>

	<!-- OPEN page -->
	<div <?php post_class('clearfix ' . $post_class_extra); ?> id="<?php the_ID(); ?>">
	
		<?php if ($sidebar_config == "both-sidebars") { ?>
			<div class="row">	
				<div class="page-content col-sm-8">
					<?php the_content(); ?>
					<div class="link-pages"><?php wp_link_pages(); ?></div>
					
					<?php if ( comments_open() && !$disable_pagecomments ) { ?>
					<div id="comment-area">
						<?php comments_template('', true); ?>
					</div>
					<?php } ?>
				</div>
					
				<aside class="sidebar left-sidebar col-sm-4">
					<?php dynamic_sidebar($left_sidebar); ?>
				</aside>
			</div>
		<?php } else { ?>
			<div class="page-content clearfix">

				<div class="container">

					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 box-1">
						<h2 style="margin-top: 0;">Start Converting Now!</h2>
						<span style="display: block; font-size: 14px; margin-bottom: 5px;">Request a</span>
						<span class="free-trial">FREE TRIAL</span>
						<span style="display: block; font-size: 14px; margin-bottom: 18px;">and 10X your sales!</span>
						<a class="sf-button standard" id="get-started" style="background: #006bb6; letter-spacing: 0px;" href="#">START NOW</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 intro-video">
						<iframe src="//player.vimeo.com/video/78642802?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="100%" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						
						<div class="share-links curved-bar-styling clearfix">
							<div class="share-text" style="line-height: 22px;">Share:</div>
							<ul class="social-icons">
								<li class="twitter">
									<a href="https://twitter.com/share?url=http://cardoneuniversity.com/&amp;text=Reach+Your+True+Potential+-" onclick="javascript:window.open(this.href,
								'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" class="product_share_twitter"><i class="fa-twitter"></i><i class="fa-twitter"></i></a>
								</li>
								<li class="facebook">
									<a href="https://www.facebook.com/sharer.php?u=http://www.grantcardone.com/cardoneuniversity" class="post_share_facebook" onclick="javascript:window.open(this.href,
								'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;"><i class="fa-facebook"></i><i class="fa-facebook"></i></a>
								</li>
								<li class="googleplus">
									<a href="https://plus.google.com/share?url=http://www.grantcardone.com/cardoneuniversity" onclick="javascript:window.open(this.href,
								'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa-google-plus"></i><i class="fa-google-plus"></i></a>
								</li>
								<li class="mail">
									<a href="mailto:?subject=Hey, Check out Cardone University for Sales Training&amp;body=Program Highlights: 600 Segments of Online Training with Testing, 52 Week Training Curriculum, 1 Year of Daily Sales Meetings, Full Accountability and Reporting in real time, New Hire, Road to Sale, Closing, Phones, Prospecting, Negotiating and more... http://cardoneuniversity.com/" class="product_share_email"><i class="ss-mail"></i><i class="ss-mail"></i></a>
								</li>
							</ul>						
						</div>

					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 box-3">
						<h2 style="margin-top: 0;">Member Login</h2>
						<form action="https://login.lightspeedvt.com/actions.cfm?lg=447&amp;v=35" method="POST">
							<input type="hidden" name="action" value="login">
							<input class="inputB" name="username" type="text" placeholder="Username">
							<input class="inputB" name="password" type="password" placeholder="Password">
							<a style="font-size: 12px;" href="http://login.lightspeedvt.com/forgot_password.cfm?lg=21" target="_blank">Forgot?</a>
							<input type="submit" class="button" value="LOGIN">
						</form>
					</div>

					<div class="clearfix"></div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="sales">
						<a class="image-video" href="#" rel="" title="Sales">
							<img src="wp-content/uploads/cardoneu-grid-sales.jpg" width="100%" alt="Sales" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Sales</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="customer-service">
						<a class="image-video" href="#" rel="" title="Customer Service">
							<img src="wp-content/uploads/cardoneu-grid-customer-service.jpg" width="100%" alt="Customer Service" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Customer Service</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="negotiations">
						<a class="image-video" href="#" rel="" title="Negotiations">
							<img src="wp-content/uploads/cardoneu-grid-negotiations.jpg" width="100%" alt="Negotiations" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Negotiations</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="mastering-phones">
						<a class="image-video" href="#" rel="" title="Mastering Phones">
							<img src="wp-content/uploads/cardoneu-grid-mastering-phones.jpg" width="100%" alt="Mastering Phones" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Mastering Phones</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="prospecting">
						<a class="image-video" href="#" rel="" title="Prospecting">
							<img src="wp-content/uploads/cardoneu-grid-prospecting.jpg" width="100%" alt="Prospecting" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Prospecting</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="communications">
						<a class="image-video" href="#" rel="" title="Communications">
							<img src="wp-content/uploads/cardoneu-grid-communications.jpg" width="100%" alt="Communications" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Communications</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="rules-of-success">
						<a class="image-video" href="#" rel="" title="Rules of Success">
							<img src="wp-content/uploads/cardoneu-grid-rules-of-success.jpg" width="100%" alt="Rules of Success" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Rules of Success</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="motivation">
						<a class="image-video" href="#" rel="" title="Motivation">
							<img src="wp-content/uploads/cardoneu-grid-motivation.jpg" width="100%" alt="Motivation" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Motivation</div>
						</a>
					</div>

					<!--<?php the_content(); ?>-->

					</div> <!-- /container -->


					<div class="row fw-row" style="padding: 35px 0 0 0;">
						<?php echo do_shortcode( '[ditty_news_ticker id="109"]' ) ?>
					</div>
			
				
				<div class="link-pages"><?php wp_link_pages(); ?></div>
				
				<?php if ( comments_open() && !$disable_pagecomments ) { ?>
					<?php if ($sidebar_config == "no-sidebars" && $pb_active == "true") { ?>
					<div id="comment-area" class="container">
					<?php } else { ?>
					<div id="comment-area">
					<?php } ?>
						<?php comments_template('', true); ?>
					</div>
				<?php } ?>				
			</div>
		<?php } ?>	
	
	<!-- CLOSE page -->
	</div>

	<?php endif; ?>
	
	<?php if ($sidebar_config == "left-sidebar") { ?>
		<aside class="sidebar left-sidebar col-sm-4">
			<?php dynamic_sidebar($left_sidebar); ?>
		</aside>
	<?php } else if ($sidebar_config == "right-sidebar") { ?>
		<aside class="sidebar right-sidebar col-sm-4">
			<?php dynamic_sidebar($right_sidebar); ?>
		</aside>
	<?php } else if ($sidebar_config == "both-sidebars") { ?>
		<aside class="sidebar right-sidebar col-sm-3">
			<?php dynamic_sidebar($right_sidebar); ?>
		</aside>
	<?php } ?>

</div>

<?php if ($sidebar_config != "no-sidebars" || $pb_active != "true") { ?>
</div>
<?php } ?>

<!--// WordPress Hook //-->
<?php get_footer(); ?>