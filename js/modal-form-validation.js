		jQuery(document).ready(function() {
			modalForms.bootstrapValidator({
				message: 'This value is not valid',
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					inf_field_FirstName: {
						group: '.col-md-6',
						validators: {
							notEmpty: {
								message: 'First Name is required'
							}
						}
					},
					inf_field_LastName: {
						group: '.col-md-6',
						validators: {
							notEmpty: {
								message: 'Last Name is required'
							}
						}
					},
					inf_field_Company: {
						group: '.col-md-6',
						validators: {
							notEmpty: {
								message: 'Organization is required'
							}
						}
					},
					inf_field_JobTitle: {
						group: '.col-md-6',
						validators: {
							notEmpty: {
								message: 'Role is required'
							}
						}
					},
					inf_field_Email: {
						group: '.col-md-6',
						validators: {
							notEmpty: {
								message: 'Email is required'
							},
							emailAddress: {
								message: 'Still not a valid email address'
							}
						}
					},
					inf_field_Phone1: {
						group: '.col-md-6',
						validators: {
							notEmpty: {
								message: 'Phone is required'
							}
						}
					}
				}
			});

		// modals.on('hide.bs.modal', function() {
		// 	modalForms.each(function() {
		// 		jQuery(this).data('bootstrapValidator').resetForm(true);
		// 	});
		// });

	});