<?php
	/*
	Template Name: Login Page
	*/
	?>

	<?php get_header(); ?>



	<!-- jQuery.js -->
	<link href="http://a.lightspeedvt.com/v1/clients/grant_cardone/counter/styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/dante-child/shadowbox/shadowbox.css">
	<script type="text/javascript" src="/wp-content/themes/dante-child/shadowbox/shadowbox.js"></script>

	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,300,400italic,400,700italic,700">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:900">


<style type="text/css">
	.modal {
		position: relative;
		display: inline;
	}
	#sb-body-inner {
		padding: 20px;
	}
	#sb-wrapper-inner {
		border: none;
		-webkit-box-shadow: 0 5px 15px rgba(0,0,0,0.5);
		box-shadow: 0 5px 15px rgba(0,0,0,0.5);
	}
	input[type="text"], input[type="password"], textarea {
		width: 100%;
		margin-bottom: 12px;
	}
	textarea {
		height: 90px;
	}
	.modal h4 {
		margin-bottom: 10px;
	}
	.modal span {
		font-size: 12px;
	}
	input[type="submit"] {
		float: right;
		margin: 6px 0 0 0;
	}
	a.image-video span.icon-play {
		position: absolute;
		left: 0;
		top: -12px;
		filter: alpha(opacity=75);
		-moz-opacity: 0.75;
		-khtml-opacity: 0.75;
		opacity: 0.75;
	}
	a.image-video:hover span.icon-play {
		filter: alpha(opacity=90);
		-moz-opacity: 0.9;
		-khtml-opacity: 0.9;
		opacity: 0.9;
	}
	a.image-video .title-video {
		width: 100%;
		font-family: 'Roboto Condensed';
		font-weight: 900;
		color: #ffffff;
		font-size: 24px;
		position: relative;
		top: -84px;
		margin: 0 auto;
		text-align: center;
		text-transform: uppercase;
		filter: alpha(opacity=90);
		-moz-opacity: 0.9;
		-khtml-opacity: 0.9;
		opacity: 0.9;
	}

	.box-1, .box-3 {
		position: relative;
		top: -150px;
	}

	.intro-video {
		position: relative;
		top: -170px;
		z-index: 1;
	}
	.intro-video iframe {
		border: 20px solid #ffffff;
	}

	@media only screen and (max-width: 767px) {
		.intro-video {
			top: -20px;
		}
		.intro-video iframe {
			border: 10px solid #ffffff;
			/*height: 260px;*/
		}			
		.box-1 {
			/*position: relative;
			top: 10px;*/
			margin-top: -70px;
			top: inherit;
		}
		.box-3 {
			top: inherit;
		}
		.fw-row {
			padding: 20px 0 40px !important;
		}
		#logo img, #logo img.retina {
			max-width: 290px;
		}
	}

	.mobile-menu-show {
		visibility: hidden !important;
	}	


	.sf-button {
		font-family: "Roboto", sans-serif !important;
		font-weight: 900 !important;
		font-style: normal !important;
	}
	.sf-button:hover{
		background: #2e2e37 !important;
	}

	.free-trial {
		display: block;
		font-family: Roboto Condensed;
		font-weight: 900;
		font-size: 48px;
		color: #f47d44;
		margin: 0 0 7px;
		letter-spacing: -2px;
		line-height: 40px;
	}
</style>

<script type="text/javascript" src="https://sp204.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=6314459ee1840d0e6e3fa0a3861222b7"></script>



<?php
$options = get_option('sf_dante_options');

$default_show_page_heading = $options['default_show_page_heading'];
$default_page_heading_bg_alt = $options['default_page_heading_bg_alt'];
$default_sidebar_config = $options['default_sidebar_config'];
$default_left_sidebar = $options['default_left_sidebar'];
$default_right_sidebar = $options['default_right_sidebar'];

$pb_active = get_post_meta($post->ID, '_spb_js_status', true);
$show_page_title = get_post_meta($post->ID, 'sf_page_title', true);
$page_title_style = get_post_meta($post->ID, 'sf_page_title_style', true);
$page_title = get_post_meta($post->ID, 'sf_page_title_one', true);
$page_subtitle = get_post_meta($post->ID, 'sf_page_subtitle', true);
$page_title_bg = get_post_meta($post->ID, 'sf_page_title_bg', true);
$fancy_title_image = rwmb_meta('sf_page_title_image', 'type=image&size=full');
$page_title_text_style = get_post_meta($post->ID, 'sf_page_title_text_style', true);
$fancy_title_image_url = "";

if ($show_page_title == "") {
	$show_page_title = $default_show_page_heading;
}
if ($page_title_bg == "") {
	$page_title_bg = $default_page_heading_bg_alt;
}
if ($page_title == "") {
	$page_title = get_the_title();
}

foreach ($fancy_title_image as $detail_image) {
	$fancy_title_image_url = $detail_image['url'];
	break;
}

if (!$fancy_title_image) {
	$fancy_title_image = get_post_thumbnail_id();
	$fancy_title_image_url = wp_get_attachment_url( $fancy_title_image, 'full' );
}

$sidebar_config = get_post_meta($post->ID, 'sf_sidebar_config', true);
$left_sidebar = get_post_meta($post->ID, 'sf_left_sidebar', true);
$right_sidebar = get_post_meta($post->ID, 'sf_right_sidebar', true);

if ($sidebar_config == "") {
	$sidebar_config = $default_sidebar_config;
}
if ($left_sidebar == "") {
	$left_sidebar = $default_left_sidebar;
}
if ($right_sidebar == "") {
	$right_sidebar = $default_right_sidebar;
}

sf_set_sidebar_global($sidebar_config);

$page_wrap_class = $post_class_extra = '';
if ($sidebar_config == "left-sidebar") {
	$page_wrap_class = 'has-left-sidebar has-one-sidebar row';
	$post_class_extra = 'col-sm-8';
} else if ($sidebar_config == "right-sidebar") {
	$page_wrap_class = 'has-right-sidebar has-one-sidebar row';
	$post_class_extra = 'col-sm-8';
} else if ($sidebar_config == "both-sidebars") {
	$page_wrap_class = 'has-both-sidebars row';
	$post_class_extra = 'col-sm-9';
} else {
	$page_wrap_class = 'has-no-sidebar';
}

$remove_breadcrumbs = get_post_meta($post->ID, 'sf_no_breadcrumbs', true);
$remove_bottom_spacing = get_post_meta($post->ID, 'sf_no_bottom_spacing', true);
$remove_top_spacing = get_post_meta($post->ID, 'sf_no_top_spacing', true);

if ($remove_bottom_spacing) {
	$page_wrap_class .= ' no-bottom-spacing';
}
if ($remove_top_spacing) {
	$page_wrap_class .= ' no-top-spacing';
}

$options = get_option('sf_dante_options');
$disable_pagecomments = false;
if (isset($options['disable_pagecomments']) && $options['disable_pagecomments'] == 1) {
	$disable_pagecomments = true;
}
?>

<?php if ($show_page_title) { ?>
<div class="container">
	<div class="row">
		<?php if ($page_title_style == "fancy") { ?>
		<?php if ($fancy_title_image_url != "") { ?>
		<div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_text_style; ?>-style fancy-image" style="background-image: url(<?php echo $fancy_title_image_url; ?>);">
			<?php } else { ?>
			<div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
				<?php } ?>
				<div class="heading-text">
					<h1 class="entry-title"><?php echo $page_title; ?></h1>
					<?php if ($page_subtitle) { ?>
					<h3><?php echo $page_subtitle; ?></h3>
					<?php } ?>
				</div>
			</div>
			<?php } else { ?>
			<div class="page-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
				<div class="heading-text">
					<h1 class="entry-title"><?php echo $page_title; ?></h1>
				</div>
				<?php 
				// BREADCRUMBS
				if (!$remove_breadcrumbs) {
					echo sf_breadcrumbs();
				}
				?>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>

	<?php if ($sidebar_config != "no-sidebars" || $pb_active != "true") { ?>
	<div class="container">
		<?php } ?>

		<div class="inner-page-wrap <?php echo $page_wrap_class; ?> clearfix">

			<?php if (have_posts()) : the_post(); ?>

				<!-- OPEN page -->
				<div <?php post_class('clearfix ' . $post_class_extra); ?> id="<?php the_ID(); ?>">

					<?php if ($sidebar_config == "both-sidebars") { ?>
					<div class="row">	
						<div class="page-content col-sm-8">
							<?php the_content(); ?>
							<div class="link-pages"><?php wp_link_pages(); ?></div>

							<?php if ( comments_open() && !$disable_pagecomments ) { ?>
							<div id="comment-area">
								<?php comments_template('', true); ?>
							</div>
							<?php } ?>
						</div>

						<aside class="sidebar left-sidebar col-sm-4">
							<?php dynamic_sidebar($left_sidebar); ?>
						</aside>
					</div>
					<?php } else { ?>
					<div class="page-content clearfix">

						<div class="container">



							<?php the_content(); ?>

						</div> <!-- /container -->
						<div class="link-pages"><?php wp_link_pages(); ?></div>

						<?php if ( comments_open() && !$disable_pagecomments ) { ?>
						<?php if ($sidebar_config == "no-sidebars" && $pb_active == "true") { ?>
						<div id="comment-area" class="container">
							<?php } else { ?>
							<div id="comment-area">
								<?php } ?>
								<?php comments_template('', true); ?>
							</div>
							<?php } ?>				
						</div>
						<?php } ?>	

						<!-- CLOSE page -->
					</div>

				<?php endif; ?>

				<?php if ($sidebar_config == "left-sidebar") { ?>
				<aside class="sidebar left-sidebar col-sm-4">
					<?php dynamic_sidebar($left_sidebar); ?>
				</aside>
				<?php } else if ($sidebar_config == "right-sidebar") { ?>
				<aside class="sidebar right-sidebar col-sm-4">
					<?php dynamic_sidebar($right_sidebar); ?>
				</aside>
				<?php } else if ($sidebar_config == "both-sidebars") { ?>
				<aside class="sidebar right-sidebar col-sm-3">
					<?php dynamic_sidebar($right_sidebar); ?>
				</aside>
				<?php } ?>

			</div>

			<?php if ($sidebar_config != "no-sidebars" || $pb_active != "true") { ?>
		</div>
		<?php } ?>

		<!--// WordPress Hook //-->
		<?php get_footer(); ?>