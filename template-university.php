<?php
	/*
	Template Name: University Page
	*/
?>

<?php get_header(); ?>



<!-- jQuery.js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="/wp-content/themes/dante-child/shadowbox/shadowbox.css">
<script type="text/javascript" src="/wp-content/themes/dante-child/shadowbox/shadowbox.js"></script>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,300,400italic,400,700italic,700">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:900">

<script type="text/javascript">
	Shadowbox.init({
	    handleOversize: "drag",
	    enableKeys: false
	});
</script>



<style type="text/css">
	.modal {
		position: relative;
		display: inline;
	}
	#sb-body-inner {
		padding: 20px;
	}
	#sb-wrapper-inner {
		border: none;
		-webkit-box-shadow: 0 5px 15px rgba(0,0,0,0.5);
		box-shadow: 0 5px 15px rgba(0,0,0,0.5);
	}
	input[type="text"], input[type="password"], textarea {
		width: 100%;
		margin-bottom: 12px;
	}
	textarea {
		height: 90px;
	}
	.modal h4 {
		margin-bottom: 10px;
	}
	.modal span {
		font-size: 12px;
	}
	input[type="submit"] {
		float: right;
		margin: 6px 0 0 0;
	}
	a.image-video span.icon-play {
		position: absolute;
		left: 0;
		top: -12px;
		filter: alpha(opacity=75);
		-moz-opacity: 0.75;
		-khtml-opacity: 0.75;
		opacity: 0.75;
	}
	a.image-video:hover span.icon-play {
		filter: alpha(opacity=90);
		-moz-opacity: 0.9;
		-khtml-opacity: 0.9;
		opacity: 0.9;
	}
	a.image-video .title-video {
		width: 100%;
		font-family: 'Roboto Condensed';
		font-weight: 900;
		color: #ffffff;
		font-size: 24px;
		position: relative;
		top: -40px;
		margin: 0 auto;
		text-align: center;
		text-transform: uppercase;
		filter: alpha(opacity=90);
		-moz-opacity: 0.9;
		-khtml-opacity: 0.9;
		opacity: 0.9;
	}

	.box-1, .box-3 {
		position: relative;
		top: -20px;
	}

	.intro-video {
		position: relative;
		top: -94px;
		z-index: 99;
	}
	.intro-video iframe {
		border: 20px solid #ffffff;
	}

	@media only screen and (max-width: 767px) {
		.intro-video {
			top: -20px;
		}
		.intro-video iframe {
			border: 10px solid #ffffff;
			height: 260px;
		}			
		.box-1 {
			position: relative;
			top: 10px;
		}
		.fw-row {
			padding: 20px 0 40px !important;
		}
		#logo img, #logo img.retina {
			max-width: 290px;
		}
	}

	.mobile-menu-show {
		visibility: hidden !important;
	}	


	.sf-button {
		font-family: "Roboto", sans-serif !important;
		font-weight: 900 !important;
		font-style: normal !important;
	}
	.sf-button:hover{
		background: #2e2e37 !important;
	}

	.free-trial {
		display: block;
		font-family: Roboto Condensed;
		font-weight: 900;
		font-size: 48px;
		color: #f47d44;
		margin: 0 0 7px;
		letter-spacing: -2px;
		line-height: 40px;
	}
</style>



<script type="text/javascript">
	$(function() {
		$('#get-started').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 490
	        });
		});
		$('#sales').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63107522?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#customer-service').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63111349?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#negotiations').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63103406?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#mastering-phones').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63110269?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#prospecting').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63108561?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#communications').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63109963?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#rules-of-success').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63105460?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
		$('#motivation').bind('click', function(event) { 
			Shadowbox.open({
	        	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63091490?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><h4>Complete the form below to activate your Free Trial.</h4><div id="ninja_forms_form_1_cont" class="ninja-forms-cont"><div id="ninja_forms_form_1_wrap" class="ninja-forms-form-wrap"><div id="ninja_forms_form_1_response_msg" style="" class="ninja-forms-response-msg "></div><form id="ninja_forms_form_1" enctype="multipart/form-data" method="post" name="" action="" class="ninja-forms-form"><input type="hidden" id="_wpnonce" name="_wpnonce" value="5eca863349"><input type="hidden" name="_wp_http_referer" value="/?page_id=147&amp;preview=true&amp;form_id=1"><input type="hidden" name="_ninja_forms_display_submit" value="1"><input type="hidden" name="_form_id" id="_form_id" value="1"><div id="ninja_forms_form_1_all_fields_wrap" class="ninja-forms-all-fields-wrap row">    <div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_6_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_6_type" value="text"><input id="ninja_forms_field_6" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_6" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="First Name *" rel="6"><input type="hidden" id="ninja_forms_field_6_label_hidden" value="First Name *"><div id="ninja_forms_field_6_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_7_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_7_type" value="text"><input id="ninja_forms_field_7" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_7" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Last Name *" rel="7"><input type="hidden" id="ninja_forms_field_7_label_hidden" value="Last Name *"><div id="ninja_forms_field_7_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_8_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_8_type" value="text"><input id="ninja_forms_field_8" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_8" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Organization *" rel="8"><input type="hidden" id="ninja_forms_field_8_label_hidden" value="Organization *"><div id="ninja_forms_field_8_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_9_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_9_type" value="text"><input id="ninja_forms_field_9" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="character(s) left" name="ninja_forms_field_9" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Role *" rel="9"><input type="hidden" id="ninja_forms_field_9_label_hidden" value="Role *"><div id="ninja_forms_field_9_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_10_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_10_type" value="text"><input id="ninja_forms_field_10" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_10" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Phone *" rel="10"><input type="hidden" id="ninja_forms_field_10_label_hidden" value="Phone *"><div id="ninja_forms_field_10_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap text-wrap label-inside col-xs-6" id="ninja_forms_field_14_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_14_type" value="text"><input id="ninja_forms_field_14" data-mask="" data-input-limit="" data-input-limit-type="char" data-input-limit-msg="" name="ninja_forms_field_14" type="text" class="ninja-forms-field  ninja-forms-req email " placeholder="Email *" rel="14"><input type="hidden" id="ninja_forms_field_14_label_hidden" value="Email *"><div id="ninja_forms_field_14_error" style="display:none;" class="ninja-forms-field-error"></div></div><div style="clear:both; height:0px;">&nbsp;</div><div class="field-wrap textarea-wrap label-inside col-xs-12" id="ninja_forms_field_11_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_11_type" value="textarea"><textarea name="ninja_forms_field_11" id="ninja_forms_field_11" class="ninja-forms-field " rel="11" data-input-limit="" data-input-limit-type="char" placeholder="Background Info" data-input-limit-msg="character(s) left"></textarea><input type="hidden" id="ninja_forms_field_11_label_hidden" placeholder="Background Info"><div id="ninja_forms_field_11_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap spam-wrap label-inside col-xs-12" id="ninja_forms_field_12_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_12_type" value="spam"><input id="ninja_forms_field_12" name="ninja_forms_field_12" type="text" class="ninja-forms-field  ninja-forms-req" placeholder="What is 4 + 4? *" rel="12"><input type="hidden" id="ninja_forms_field_12_label_hidden" value="What is 4 + 4? *"><div id="ninja_forms_field_12_error" style="display:none;" class="ninja-forms-field-error"> </div></div><div class="field-wrap submit-wrap label-above" id="ninja_forms_field_13_div_wrap" data-visible="1"><input type="hidden" id="ninja_forms_field_13_type" value="submit"><div id="nf_submit_1"><input type="submit" name="_ninja_forms_field_13" class="ninja-forms-field " id="ninja_forms_field_13" value="Submit" rel="13"></div><div id="nf_processing_1" style="display:none;"><input type="submit" name="" class="ninja-forms-field " value="Processing" rel="13" disabled=""></div><div id="ninja_forms_field_13_error" style="display:none;" class="ninja-forms-field-error"> </div></div></div></form></div></div></div>',
	            player:  "html",
	            height: 760
	        });
		});
	});
</script>

<script>
	function validateForm() {
		var x=document.forms["insightly_web_to_contact"]["FirstName"].value;
		if (x==null || x=="") {
			alert("First Name must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["LastName"].value;
		if (x==null || x=="") {
			alert("Last Mame must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["Organization"].value;
		if (x==null || x=="") {
			alert("Organization must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["Role"].value;
		if (x==null || x=="") {
			alert("Role must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["emails[0].Value"].value;
		if (x==null || x=="") {
			alert("Email must be filled out.");
			return false;
		}
		var x=document.forms["insightly_web_to_contact"]["phones[0].Value"].value;
		if (x==null || x=="") {
			alert("Phone must be filled out.");
			return false;
		}
	}
</script>

	

<?php
	$options = get_option('sf_dante_options');
	
	$default_show_page_heading = $options['default_show_page_heading'];
	$default_page_heading_bg_alt = $options['default_page_heading_bg_alt'];
	$default_sidebar_config = $options['default_sidebar_config'];
	$default_left_sidebar = $options['default_left_sidebar'];
	$default_right_sidebar = $options['default_right_sidebar'];
	
	$pb_active = get_post_meta($post->ID, '_spb_js_status', true);
	$show_page_title = get_post_meta($post->ID, 'sf_page_title', true);
	$page_title_style = get_post_meta($post->ID, 'sf_page_title_style', true);
	$page_title = get_post_meta($post->ID, 'sf_page_title_one', true);
	$page_subtitle = get_post_meta($post->ID, 'sf_page_subtitle', true);
	$page_title_bg = get_post_meta($post->ID, 'sf_page_title_bg', true);
	$fancy_title_image = rwmb_meta('sf_page_title_image', 'type=image&size=full');
	$page_title_text_style = get_post_meta($post->ID, 'sf_page_title_text_style', true);
	$fancy_title_image_url = "";
	
	if ($show_page_title == "") {
		$show_page_title = $default_show_page_heading;
	}
	if ($page_title_bg == "") {
		$page_title_bg = $default_page_heading_bg_alt;
	}
	if ($page_title == "") {
		$page_title = get_the_title();
	}
	
	foreach ($fancy_title_image as $detail_image) {
		$fancy_title_image_url = $detail_image['url'];
		break;
	}
									
	if (!$fancy_title_image) {
		$fancy_title_image = get_post_thumbnail_id();
		$fancy_title_image_url = wp_get_attachment_url( $fancy_title_image, 'full' );
	}
	
	$sidebar_config = get_post_meta($post->ID, 'sf_sidebar_config', true);
	$left_sidebar = get_post_meta($post->ID, 'sf_left_sidebar', true);
	$right_sidebar = get_post_meta($post->ID, 'sf_right_sidebar', true);
	
	if ($sidebar_config == "") {
		$sidebar_config = $default_sidebar_config;
	}
	if ($left_sidebar == "") {
		$left_sidebar = $default_left_sidebar;
	}
	if ($right_sidebar == "") {
		$right_sidebar = $default_right_sidebar;
	}
	
	sf_set_sidebar_global($sidebar_config);
	
	$page_wrap_class = $post_class_extra = '';
	if ($sidebar_config == "left-sidebar") {
	$page_wrap_class = 'has-left-sidebar has-one-sidebar row';
	$post_class_extra = 'col-sm-8';
	} else if ($sidebar_config == "right-sidebar") {
	$page_wrap_class = 'has-right-sidebar has-one-sidebar row';
	$post_class_extra = 'col-sm-8';
	} else if ($sidebar_config == "both-sidebars") {
	$page_wrap_class = 'has-both-sidebars row';
	$post_class_extra = 'col-sm-9';
	} else {
	$page_wrap_class = 'has-no-sidebar';
	}
	
	$remove_breadcrumbs = get_post_meta($post->ID, 'sf_no_breadcrumbs', true);
	$remove_bottom_spacing = get_post_meta($post->ID, 'sf_no_bottom_spacing', true);
	$remove_top_spacing = get_post_meta($post->ID, 'sf_no_top_spacing', true);
	
	if ($remove_bottom_spacing) {
	$page_wrap_class .= ' no-bottom-spacing';
	}
	if ($remove_top_spacing) {
	$page_wrap_class .= ' no-top-spacing';
	}
	
	$options = get_option('sf_dante_options');
	$disable_pagecomments = false;
	if (isset($options['disable_pagecomments']) && $options['disable_pagecomments'] == 1) {
	$disable_pagecomments = true;
	}
?>

<?php if ($show_page_title) { ?>
<div class="container">
	<div class="row">
		<?php if ($page_title_style == "fancy") { ?>
		<?php if ($fancy_title_image_url != "") { ?>
		<div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_text_style; ?>-style fancy-image" style="background-image: url(<?php echo $fancy_title_image_url; ?>);">
		<?php } else { ?>
		<div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
		<?php } ?>
			<div class="heading-text">
				<h1 class="entry-title"><?php echo $page_title; ?></h1>
				<?php if ($page_subtitle) { ?>
				<h3><?php echo $page_subtitle; ?></h3>
				<?php } ?>
			</div>
		</div>
		<?php } else { ?>
		<div class="page-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
			<div class="heading-text">
				<h1 class="entry-title"><?php echo $page_title; ?></h1>
			</div>
			<?php 
				// BREADCRUMBS
				if (!$remove_breadcrumbs) {
					echo sf_breadcrumbs();
				}
			?>
		</div>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php if ($sidebar_config != "no-sidebars" || $pb_active != "true") { ?>
<div class="container">
<?php } ?>

<div class="inner-page-wrap <?php echo $page_wrap_class; ?> clearfix">
		
	<?php if (have_posts()) : the_post(); ?>

	<!-- OPEN page -->
	<div <?php post_class('clearfix ' . $post_class_extra); ?> id="<?php the_ID(); ?>">
	
		<?php if ($sidebar_config == "both-sidebars") { ?>
			<div class="row">	
				<div class="page-content col-sm-8">
					<?php the_content(); ?>
					<div class="link-pages"><?php wp_link_pages(); ?></div>
					
					<?php if ( comments_open() && !$disable_pagecomments ) { ?>
					<div id="comment-area">
						<?php comments_template('', true); ?>
					</div>
					<?php } ?>
				</div>
					
				<aside class="sidebar left-sidebar col-sm-4">
					<?php dynamic_sidebar($left_sidebar); ?>
				</aside>
			</div>
		<?php } else { ?>
			<div class="page-content clearfix">

				<div class="container">

			
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 intro-video">
						<div class="video-container">
						<iframe src="//player.vimeo.com/video/63090745" width="100%" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="share-links curved-bar-styling clearfix">
							<div class="share-text" style="line-height: 22px;">Share:</div>
							<ul class="social-icons">
								<li class="twitter">
									<a href="https://twitter.com/share?url=http://cardoneuniversity.com/&amp;text=Reach+Your+True+Potential+-" onclick="javascript:window.open(this.href,
								'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;" class="product_share_twitter"><i class="fa-twitter"></i><i class="fa-twitter"></i></a>
								</li>
								<li class="facebook">
									<a href="https://www.facebook.com/sharer.php?u=http://www.grantcardone.com/cardoneuniversity" class="post_share_facebook" onclick="javascript:window.open(this.href,
								'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;"><i class="fa-facebook"></i><i class="fa-facebook"></i></a>
								</li>
								<li class="googleplus">
									<a href="https://plus.google.com/share?url=http://www.grantcardone.com/cardoneuniversity" onclick="javascript:window.open(this.href,
								'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa-google-plus"></i><i class="fa-google-plus"></i></a>
								</li>
								<li class="mail">
									<a href="mailto:?subject=Hey, Check out Cardone on Demand for Sales Training&amp;body=Program Highlights: 600 Segments of Online Training with Testing, 52 Week Training Curriculum, 1 Year of Daily Sales Meetings, Full Accountability and Reporting in real time, New Hire, Road to Sale, Closing, Phones, Prospecting, Negotiating and more... http://cardoneuniversity.com/" class="product_share_email"><i class="ss-mail"></i><i class="ss-mail"></i></a>
								</li>
							</ul>						
						</div>

					</div>

					
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 box-1">
						<h2 style="margin-top: 0;">Start Converting Now!</h2>
						<span style="display: block; font-size: 14px; margin-bottom: 5px;">Request a</span>
						<span class="free-trial">FREE TRIAL</span>
						<span style="display: block; font-size: 14px; margin-bottom: 18px;">and 10X your sales!</span>
						<a class="sf-button standard" id="get-started" style="background: #006bb6; letter-spacing: 0px;" href="#">START NOW</a>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 box-3">
						<h2 style="margin-top: 0;">Member Login</h2>
						<form action="https://login.lightspeedvt.com/actions.cfm?lg=447&amp;v=35" method="POST">
							<input type="hidden" name="action" value="login">
							<input class="inputB" name="username" type="text" placeholder="Username">
							<input class="inputB" name="password" type="password" placeholder="Password">
							<a style="font-size: 12px;" href="http://login.lightspeedvt.com/forgot_password.cfm?lg=21" target="_blank">Forgot?</a>
							<input type="submit" class="button" value="LOGIN">
						</form>
					</div>


					<div class="clearfix"></div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="sales">
						<a class="image-video" href="#" rel="" title="Sales">
							<img src="wp-content/uploads/cardone-on-demand-page-1.jpg" width="100%" alt="Sales" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">New Hire</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="customer-service">
						<a class="image-video" href="#" rel="" title="Customer Service">
							<img src="wp-content/uploads/cardone-on-demand-page-2.jpg" width="100%" alt="Customer Service" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Sales Meeting</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="negotiations">
						<a class="image-video" href="#" rel="" title="Negotiations">
							<img src="wp-content/uploads/cardone-on-demand-page-3.jpg" width="100%" alt="Negotiations" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Road to Sale</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="mastering-phones">
						<a class="image-video" href="#" rel="" title="Mastering Phones">
							<img src="wp-content/uploads/cardone-on-demand-page-4.jpg" width="100%" alt="Mastering Phones" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Objections</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="prospecting">
						<a class="image-video" href="#" rel="" title="Prospecting">
							<img src="wp-content/uploads/cardone-on-demand-page-5.jpg" width="100%" alt="Prospecting" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Promotions</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="communications">
						<a class="image-video" href="#" rel="" title="Communications">
							<img src="wp-content/uploads/cardone-on-demand-page-6.jpg" width="100%" alt="Communications" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Phones & BDC</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="rules-of-success">
						<a class="image-video" href="#" rel="" title="Rules of Success">
							<img src="wp-content/uploads/cardone-on-demand-page-7.jpg" width="100%" alt="Rules of Success" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Prospecting</div>
						</a>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" id="motivation">
						<a class="image-video" href="#" rel="" title="Motivation">
							<img src="wp-content/uploads/cardone-on-demand-page-8.jpg" width="100%" alt="Motivation" />
							<span class="icon-play"><img src="wp-content/uploads/cardoneu-grid-play.png" width="100%" alt="Play"></span>
							<div class="title-video">Closing</div>
						</a>
					</div>

					<!--<?php the_content(); ?>-->

					</div> <!-- /container -->


					<div class="row fw-row" style="padding: 35px 0 0 0;">
						<?php echo do_shortcode( '[ditty_news_ticker id="109"]' ) ?>
					</div>
			
				
				<div class="link-pages"><?php wp_link_pages(); ?></div>
				
				<?php if ( comments_open() && !$disable_pagecomments ) { ?>
					<?php if ($sidebar_config == "no-sidebars" && $pb_active == "true") { ?>
					<div id="comment-area" class="container">
					<?php } else { ?>
					<div id="comment-area">
					<?php } ?>
						<?php comments_template('', true); ?>
					</div>
				<?php } ?>				
			</div>
		<?php } ?>	
	
	<!-- CLOSE page -->
	</div>

	<?php endif; ?>
	
	<?php if ($sidebar_config == "left-sidebar") { ?>
		<aside class="sidebar left-sidebar col-sm-4">
			<?php dynamic_sidebar($left_sidebar); ?>
		</aside>
	<?php } else if ($sidebar_config == "right-sidebar") { ?>
		<aside class="sidebar right-sidebar col-sm-4">
			<?php dynamic_sidebar($right_sidebar); ?>
		</aside>
	<?php } else if ($sidebar_config == "both-sidebars") { ?>
		<aside class="sidebar right-sidebar col-sm-3">
			<?php dynamic_sidebar($right_sidebar); ?>
		</aside>
	<?php } ?>

</div>

<?php if ($sidebar_config != "no-sidebars" || $pb_active != "true") { ?>
</div>
<?php } ?>

<!--// WordPress Hook //-->
<?php get_footer(); ?>